﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    //deklarasi class
    public BallFollow ball;

    [SerializeField]
    private GameObject bomb;

    bool bombSpawned = false;
    bool bombShuffled = false;
    // Batas atas dan bawah game scene (Batas bawah menggunakan minus (-))
    public float yBoundary = 8.0f;
    // Batas kiri dan kanan game scene (Batas kiri menggunakan minus (-))
    public float xBoundary = 15.5f;

    // jumlah bomb satu saja
    private int maxBomb = 1;
    private int minBomb = 1;
    public int currentBomb = 0;

    // detik waktu reactive
    [SerializeField]
    private float timeReactiveBomb = 10.0f;
    private float timer = 0.0f;

    // lives system
    public int reduceLive = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        maxBomb = Random.Range(minBomb, maxBomb);
    }

    // Update is called once per frame
    void Update()
    {
        SpawnBomb();
        
        Debug.Log(bombShuffled);
        if (currentBomb == 0)
        {
            InvokeRepeating("ReactiveBomb", timeReactiveBomb, timeReactiveBomb);
        }
        else
        {
            Invoke("ReactiveNotExplodedBomb", timeReactiveBomb);
            timer += 1.0f * Time.deltaTime;

            if (timer >= 10.0f)
            {
                bombShuffled = false;
                timer = 0.0f;
            }
        }
    }

    void SpawnBomb()
    {
        if (!bombSpawned)
        {
            Vector3 bombPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), 0f);

            if (currentBomb < maxBomb)
            {
                if (bombPosition == ball.transform.position)
                {
                    bombPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), 0f);
                }
                else
                {
                    bombSpawned = false;
                    Instantiate(bomb, bombPosition, Quaternion.Euler(0, 0, Random.Range(0, 360)));
                    currentBomb++;
                }
            }
            else
            {
                bombSpawned = true;
            }
        }
    }

    void ReactiveBomb()
    {
        GameObject[] gameObjectBomb = GameObject.FindGameObjectsWithTag("ExplodedBomb");
        foreach (GameObject gob in gameObjectBomb)
        {
            gob.GetComponent<SpriteRenderer>().enabled = true;
            gob.GetComponent<CircleCollider2D>().enabled = true;
            Vector3 explodedBombPosition = gob.transform.position;
            explodedBombPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
            if (explodedBombPosition == ball.transform.position)
            {
                explodedBombPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
            }
            else
            {
                gob.transform.position = explodedBombPosition;
                //Debug.Log(explodedBombPosition);
                currentBomb++;
                gob.tag = "Bomb";
            }
        }
    }

    void ReactiveNotExplodedBomb()
    {
        GameObject[] notexplodeBomb = GameObject.FindGameObjectsWithTag("Bomb");
        foreach (GameObject neb in notexplodeBomb)
        {
            if (!bombShuffled)
            {
                Vector3 notexplodeBombPosition = neb.transform.position;
                notexplodeBombPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
                if (notexplodeBombPosition == ball.transform.position)
                {
                    notexplodeBombPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
                }
                else
                {
                    neb.transform.position = notexplodeBombPosition;
                    bombShuffled = true;
                    //Debug.Log(notexplodeBombPosition);
                }
            }
        }
    }

    public void Live()
    {
        LiveManager.live -= reduceLive;
        currentBomb--;
        //Debug.Log(myscore);
    }
}
