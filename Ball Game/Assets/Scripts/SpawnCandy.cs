﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpawnCandy : MonoBehaviour
{
    //deklarasi class
    public BallFollow ball;
   
    [SerializeField]
    private GameObject candy;
    [SerializeField]
    private GameObject premiumCandy;

    bool candySpawned = false;
    // Batas atas dan bawah game scene (Batas bawah menggunakan minus (-))
    public float yBoundary = 8.0f;
    // Batas kiri dan kanan game scene (Batas kiri menggunakan minus (-))
    public float xBoundary = 15.5f;

    // batas jumlah candy
    public int maxCandy = 15;
    public int minCandy = 8; 
    public int currentCandy = 0;

    // detik waktu reactive
    [SerializeField]
    private float timeReactiveCandy = 3.0f;

    // scoring system
    public int addScore = 1;
    public int addPremiumScore = 3;

    private void Start()
    {
        maxCandy = Random.Range(minCandy, maxCandy);
        //setelah timeReactiveCandy detik object yang pernah ditabrak diaktifkan kembali
        InvokeRepeating("Reactive", timeReactiveCandy, timeReactiveCandy);
    }

    void Update()
    {
        
        Spawn();
        //Debug.Log(candySpawned);
        //Debug.Log(currentCandy);
                
    }

    void Spawn()
    {
        if (!candySpawned)
        {
            Vector3 candyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), 0f);
            
            if (currentCandy < maxCandy)
            {
                if (candyPosition == ball.transform.position)
                {
                    candyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), 0f);
                }
                else
                {
                    candySpawned = false;
                    Instantiate(candy, candyPosition, Quaternion.Euler(0, 0, Random.Range(0, 360)));
                    if (currentCandy < maxCandy - (minCandy / 3))
                    {
                        candyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), 0f);
                        if (candyPosition == ball.transform.position)
                        {
                            candyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), 0f);
                        }
                        Instantiate(premiumCandy, candyPosition, Quaternion.Euler(0, 0, Random.Range(0, 360)));
                        currentCandy++;
                    }
                    currentCandy++;
                }
            }
            else
            {
                candySpawned = true;
            }
        }
    }

    void Reactive()
    {
        GameObject[] gameObjectCandy = GameObject.FindGameObjectsWithTag("EatenCandy");
        foreach (GameObject go in gameObjectCandy)
        {
            //Debug.Log(gameObjectCandy);
            go.GetComponent<SpriteRenderer>().enabled = true;
            go.GetComponent<CapsuleCollider2D>().enabled = true;
            Vector3 eatenCandyPosition = go.transform.position;
            eatenCandyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
            go.transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0, 360));
            if (eatenCandyPosition == ball.transform.position)
            {
                eatenCandyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
            }
            else
            {
                go.transform.position = eatenCandyPosition;
                //Debug.Log(eatenCandyPosition);
                currentCandy++;
                go.tag = "Candy";
            }
         }

        GameObject[] gameObjectPremiumCandy = GameObject.FindGameObjectsWithTag("EatenPremiumCandy");
        foreach (GameObject pc in gameObjectPremiumCandy)
        {
            //Debug.Log(gameObjectCandy);
            pc.GetComponent<SpriteRenderer>().enabled = true;
            pc.GetComponent<CapsuleCollider2D>().enabled = true;
            Vector3 eatenPremiumCandyPosition = pc.transform.position;
            eatenPremiumCandyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
            pc.transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0, 360));
            if (eatenPremiumCandyPosition == ball.transform.position)
            {
                eatenPremiumCandyPosition = new Vector3(Random.Range(-xBoundary, xBoundary), Random.Range(-yBoundary, yBoundary), Random.Range(0, 360));
            }
            else
            {
                pc.transform.position = eatenPremiumCandyPosition;
                //Debug.Log(eatenCandyPosition);
                currentCandy++;
                pc.tag = "PremiumCandy";
            }
        }
    }

    public void Score()
    {
        ScoreManager.score += addScore;
        currentCandy--;
        //Debug.Log(myscore);
    }

    public void PremiumScore()
    {
        ScoreManager.score += addPremiumScore;
        currentCandy--;
        //Debug.Log(myscore);
    }

}