﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class LiveManager : MonoBehaviour
{
    public static int live;
    public float delayTime = 5.0f;
    public BallFollow ball;
    
    Text text;
    public GameObject gameOver;

    void Awake()
    {
        text = GetComponent<Text>();
        live = 3;
    }


    void Update()
    {
        text.text = "Lives: " + live;
        if (live == 0)
        {
            gameOver.SetActive(true);
            ball.enabled = false;
            Invoke("GameOver", delayTime);
        }
    }

    void GameOver()
    {
        gameOver.SetActive(false);
        SceneManager.LoadScene("Scene10");
    }
}