﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallFollow : MonoBehaviour
{
    public SpawnCandy spawnedCandy;
    public SpawnEnemy spawnedEnemy;

    // Animasi Player
    Animator anim;
    bool isGotCandy = false;

    // Rigidbody 2D bola
    private Rigidbody2D rigidBody2D;

    // Batas atas dan bawah game scene (Batas bawah menggunakan minus (-))
    public float yBoundary = 8.0f;
    // Batas kiri dan kanan game scene (Batas kiri menggunakan minus (-))
    public float xBoundary = 15.5f;
    // kecepatan bola
    private float moveSpeed = 10.0f;
    
    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 tmpDir = cursorPos - (Vector2)transform.position;
        transform.position = Vector2.Lerp(transform.position, cursorPos, moveSpeed / tmpDir.magnitude * Time.deltaTime);
       
        // Dapatkan posisi bola sekarang.
        Vector3 position = transform.position;

        // Jika posisi bola melewati batas atas (yBoundary), kembalikan ke batas atas tersebut.
        if (position.y > yBoundary)
        {
            position.y = yBoundary;
        }

        // Jika posisi bola melewati batas bawah (-yBoundary), kembalikan ke batas atas tersebut.
        else if (position.y < -yBoundary)
        {
            position.y = -yBoundary;
        }

        // Jika posisi bola melewati batas kanan (xBoundary), kembalikan ke batas kanan tersebut.
        if (position.x > xBoundary)
        {
            position.x = xBoundary;
        }

        // Jika posisi bola melewati batas kiri (-xBoundary), kembalikan ke batas kiri tersebut.
        else if (position.x < -xBoundary)
        {
            position.x = -xBoundary;
        }

        // Masukkan kembali posisinya ke transform.
        transform.position = position;

        //Animasi
        Animating();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Candy")
        {
            collision.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            collision.gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            collision.gameObject.tag = "EatenCandy";
            isGotCandy = true;
            spawnedCandy.Score();
        }

        else if (collision.gameObject.tag == "PremiumCandy")
        {
            collision.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            collision.gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
            collision.gameObject.tag = "EatenPremiumCandy";
            isGotCandy = true;
            spawnedCandy.PremiumScore();
        }

        else if (collision.gameObject.tag == "Bomb")
        {
            collision.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            collision.gameObject.GetComponent<CircleCollider2D>().enabled = false;
            collision.gameObject.tag = "ExplodedBomb";
            spawnedEnemy.Live();
        }
    }

    //untuk playing animasinya
    public void Animating()
    {
        anim.SetBool("isGotCandy", isGotCandy);
        if (isGotCandy)
        {
            isGotCandy = !isGotCandy;
        }
    }
}
